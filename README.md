https://eins_1.goorm.io

서버를 가동하기 위해서는+ mongodb와 node.js, gulp가 설치되어 있어야 합니다.

node.js 는 v0.12 이상 버전이어야 합니다.
### Node.js v0.12: Installation Shell script


#### Using Ubuntu
    curl -sL https://deb.nodesource.com/setup_0.12 | sudo -E bash -
    sudo apt-get install -y nodejs

#### Using Debian, as root
    curl -sL https://deb.nodesource.com/setup_0.12 | bash -
    apt-get install -y nodejs


몽고DB 데몬을 백그라운드에서 돌리는 명령어는 다음과 같습니다.(*nix 시스템기준)

    nohup mongod > /var/log/mongodb/mongodb.nohup.log 2>&1 &

gulp 설치방법

	npm install -g gulp
    
프로젝트를 내려받은 뒤, npm_module 파일을 삭제한 뒤 다음과 같은 명령어를 입력합니다.

	npm install

서버 가동 명령어는 다음과 같습니다.

    gulp
    
종료를 원할때는 Ctrl + C키로 서버를 종료시킵니다.
