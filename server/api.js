(function() {

  'use strict';
  /*
  var app = require('../app');
  var express = app.express;
  var router = express.Router();*/
  
  var express = require('express');
  var router = express.Router();
  var fs = require('fs');
  var path = require('path');

  var fb_root_dir_name = 'fb_target_data';
//File browser root directory name


  // /* GET home page. */
  // router.get('/', function(req, res) {
  //   res.render('fb');
  //   console.log('directory name is :' + __dirname);
  // });
  router.post('/save', function(req, res) {
    console.log('/api/save POST request');
    var _p;
    var file_path = req.body.fpath.toString();
    var content = req.body.contents.toString();
    _p = path.resolve(__dirname, '..', fb_root_dir_name);
    // console.log(_p);
    // console.log(file_path);
    //console.log(content);
    // var pos = file_path.search(_p);
    // console.log('pos is '+pos);
    fs.writeFile(file_path, content, function(err) {
      if(err) throw err;
      console.log('It\' saved!');
    });
  });
    router.post('/rename', function(req, res) {
        console.log('api/rename POST request');
        var oldPath = req.body.oldPath;
        var newPath = req.body.newPath;
        fs.access(oldPath, fs.F_OK | fs.R_OK | fs.W_OK, function(err) {
            if(err){
                console.log('file:'+oldPath+' is not accessible');
                res.sendStatus(401);//no auth
                //throw err;  
            } else {
                    fs.access(newPath, fs.F_OK, function(err) {
                    if(err) {
                        fs.rename(oldPath, newPath, function(err) {
                        if(err){
                            res.sendStatus(406);//(forbidden)renaming failure
                            throw err;
                        } 
                        console.log('rename successfully');
                        console.log('oldPath:'+oldPath);
                        console.log('newPath:'+newPath);
                        res.sendStatus(200);//successfully handled
                        });
                        
                    } else {
                        res.sendStatus(403);
                        console.log('file:'+newPath+' already exists');
                        }
                    });
                }
            });  
    });

  router.get('/del', function(req, res) {
    console.log('/api/del GET request');
    var file_path = req.query.fpath;
    console.log(file_path);
    fs.unlink(file_path, function(err) {
      if(err){
	console.log(file_path + 'file unlink failure');
	}
      console.log(file_path + 'deleted successfully');
    })

  });

  /* Serve the Tree */
  router.get('/tree', function(req, res) {
    var _p;
    if (req.query.id == 1) {
      // console.log("debug in /api/tree id==1");
      _p = path.resolve(__dirname, '..', fb_root_dir_name);
      //Configuration root path here!
      //@param3 of path.resolve function is directory path definition
      // current directory('./') means below codigmmission dir
      // console.log("debug in /api/tree id==1 (path resolved)");
      processReq(_p, res);

    } else {
      if (req.query.id) {
        // console.log("debug in /api/tree id>1");
        _p = req.query.id;
        processReq(_p, res);
      } else {
        res.json(['No valid data found']);
      }
    }
  });

  /* Serve a Resource */
  router.get('/resource', function(req, res) {
    fs.access(req.query.resource, fs.F_OK | fs.R_OK, function(err) {
      if(err){
        res.send('No Valid file');
//       throw err;  
      }
      else{
        res.send(fs.readFileSync(req.query.resource, 'UTF-8'));
      }
    });
  });

        
    
    

  function processReq(_p, res) {
    var resp = [];
    fs.readdir(_p, function(err, list) {
      if(err) {
        console.error(err);
        res.json(resp);
      }  
      else{
        for (var i = list.length - 1; i >= 0; i--) {
            resp.push(processNode(_p, list[i]));
        }
        res.json(resp);
      }
    });
  }

  function processNode(_p, f) {
    var s = fs.statSync(path.join(_p, f));
    return {
      "id": path.join(_p, f),
      "text": f,
      "icon" : s.isDirectory() ? '' : 'jstree-file',
      "state": {
        "opened": false,
        "disabled": false,
        "selected": false
      },
      "li_attr": {
        "base": path.join(_p, f),
        "isLeaf": !s.isDirectory()
      },
      "children": s.isDirectory()
    };
  }

  module.exports = router;

}());
