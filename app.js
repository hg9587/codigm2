var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var crypto = require('crypto');

var routes = require('./routes/index');
var users = require('./routes/users');
var login = require('./routes/login');
var login_check = require('./routes/login_check');
var register = require('./routes/register');
var register_check = require('./routes/register_check');
var logout = require('./routes/logout');
var main = require('./routes/main');
var dump = require('./routes/dump');
var socketio = require('socket.io');

var server = null;
var io = null;

var api = require('./server/api');
// var fileBrowser = require('./server/routes');//file browser module
var fileBrowser = require('./server/fileBrowser');//file browser module

var tetris = require('./routes/tetris');

var app = express();
//서버 로직에 필요한 모듈 require하는 부분


function hashPW(pwd) {
	return crypto.createHash('sha256').update(pwd).digest('base64').toString();
};
exports.hashPW = hashPW;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser('MagicStrinG'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'ichbineinstrasse', cookie: { maxAge: 3600000 }, resave: true, saveUninitialized: true }));//session using
//app.use(session());

app.use('/', routes);
app.use('/fb', fileBrowser); //fileBrowser path = localhost/fb
app.use('/api', api); //fileBrowser support apis
app.use('/users', users);
app.use('/login',login);
app.use('/login_check', login_check);
app.use('/register', register);
app.use('/register_check', register_check);
app.use('/main', main);
app.use('/logout', logout);
app.use('/dev', dump);
app.use('/tetris', tetris);

// url 상에서 요청이  domain_name/@param1 형태로 오면, var @param의 script를 실행한다.

mongoose.connect('mongodb://localhost/codigm');
//데이터베이스 연결 시도
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongodb connection error:'));
//에러 핸들러 설정
db.once('open', function() {
	console.log('mongodb connection established successfully');
	//디비 연결 성공 메시지 출력
});

var accountSchema = mongoose.Schema({
	id: String,
	passwd: String
});
accountSchema.methods.dump = function() {
	var message = "id: "+ this.id +", passwd: " + this.passwd;
	console.log(message);
}

var Account =  mongoose.model('account', accountSchema);

exports.Account = Account;
//위외 같은 선언을 하면 다른 파일에서 다음과 같은 syntax로 접근이 가능하다
/*
// ex.js file
var share_target = 'some data';
exports.new_name = share_target;
// new.js file
var ex = require('ex');
console.log(ex.new_name);
*/

//einstrasse아이디가 여러개가 존재하면 하나 빼고 다 지워라
Account.find({ id: "einstrasse" }, function(err, docs) {
	var length = docs.length;
	if(length === 1)
		return;
	for( i = 1 ; i < length ; i++) {
		//docs[i].dump();
		docs[i].remove();
	}
		
});

//einstrasse 계정이 없으면 생성하라
Account.find({ id: "einstrasse" }, function(err, docs) {
	var length = docs.length;
	if(length === 0) {
		var einstrasse = new Account({ id: 'einstrasse', passwd: hashPW('12321')});
		einstrasse.save(function (err, instance) {
			if(err) return console.error(err);
			instance.dump();
			console.log("Saved Successfully");
		});
	}
})


// einstrasse.save(function (err, instance) {
// 	if (err) return console.error(err);
// 	instance.dump();
// 	console.log('Saved successfully');
// })

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
