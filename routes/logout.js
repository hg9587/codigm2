var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	if(req.session.user){
		console.log('log out successfully');
		req.session.destroy(function() {
			res.redirect('/');
		});
		
// 		var response = "<script> alert('you already have session');" +
// 			"location.href='/main'</script>";
// 		res.send(response);
	}
	else {
		console.log('No-session user accessed to logout page');
		var response = "<script> alert('You have no session'); location.href='/';</script> ";
		res.send(response);
	}
});
module.exports = router;
