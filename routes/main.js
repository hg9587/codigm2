var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var sess = "";
	var log = "login";
	if(req.session.user){
		sess = req.session.user.id.toString();
		sess += "님 환영합니다. ";
		log = "logout";
	}
	console.log(req.session.user);
	if(req.session.user) {
  		res.render('tetris', { title: 'Main페이지',
					  userId: sess,
						   logger: log});
	} else {
		var response = "<script>alert('Access Denied. Please Login');" +
			"location.href='/login' </script>" +
			"Please Login";
		res.send(response);
	}
});

module.exports = router;
