var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	var sess = "";
	var log = "login";
	if(req.session.user){
		sess = req.session.user.id;
		sess += "님 환영합니다. ";
		log = "logout";
	}
	res.render('register', { title: 'Register',
					  userId: sess , 
						   logger: log});
});

module.exports = router;
